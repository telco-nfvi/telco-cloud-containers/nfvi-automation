#!/usr/bin/python3
import sys
import logging
import json
import time
import requests
from requests.structures import CaseInsensitiveDict
from requests.packages.urllib3.exceptions import InsecureRequestWarning


__AUTHOR__ = "Veyis Ceylan(vceylan@vmware.com)"
__VERSION__ = 0.0001

logging.basicConfig(stream=sys.stdout,
                    format='%(asctime)s: %(module)s:  %(funcName)s:  [%(levelname)s]: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)

headers = CaseInsensitiveDict()
headers["Content-Type"] = "application/json"
headers["Accept"] = "application/json"
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class TCAConfigurator:
    """
       Parameters for TCAConfigurator Class
         tca_controller = The URL for TCA Manager
         tca_controller_user = The TCA Manager User
         tca_controller_password = The TCA Manager Password
         data = The input variables in YAML format for rendering API calls for TCA Objects
         log_enable = Enable logging while rendering API calls for TCA Objects
    """
    def __init__(self, tca_controller=None, tca_controller_user=None, tca_controller_password=None, data=None, log_enable=False):
        self.tca_controller = tca_controller
        self.tca_controller_user = tca_controller_user
        self.tca_controller_password = tca_controller_password
        self.data = data
        self.log_enable = log_enable
        logging.info("TCA Configurator Agent is created")

        payload = json.dumps({
            "username": self.tca_controller_user,
            "password": self.tca_controller_password
        })

        token_http_url = self.tca_controller + '/hybridity/api/sessions'
        token_resp = requests.post(token_http_url, headers=headers, data=payload, verify=False)
        if token_resp.status_code != 200:
            logging.error('TCA API Token is not generated')
            exit(4)
        headers['x-hm-authorization'] = token_resp.headers['x-hm-authorization']

    @staticmethod
    def tca_object_exist_check(tca_object_name, tca_object_list, tca_object_feature):
        for item in tca_object_list:
            if tca_object_name == item[tca_object_feature]:
                return item
            else:
                continue
        return False

    @staticmethod
    def vim_object_exist_check(vim_object_name, vim_object_list, vim_object_feature):
        for item in vim_object_list['items']:
            if vim_object_name == item[vim_object_feature]:
                return item
            else:
                continue
        return False

    @staticmethod
    def repository_object_exist_check(repository_object_name, repository_object_list, repository_object_feature):
        for item in repository_object_list['extensions']:
            if repository_object_name == item[repository_object_feature]:
                return item
            else:
                continue
        return False

    def management_cluster_template_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: TCA Management Cluster Template
        """
        for management_template in self.data['caas_templates']['management_clusters']:
            if self.log_enable:
                logging.info('TCA Management Cluster Template ' + str(management_template['name']))

            base_http_url = self.tca_controller + '/hybridity/api/infra/cluster-templates'
            management_template_resp = requests.get(base_http_url, headers=headers, verify=False)

            if management_template_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Management Cluster Template Query')
                exit(3)

            tca_object = TCAConfigurator.tca_object_exist_check(management_template['name'], management_template_resp.json(), 'name')

            # This is for rendering the POST or PATCH API Commands.
            if operator == 'create':
                management_template_body = {
                    "clusterType": "MANAGEMENT",
                    "clusterConfig": {
                            "kubernetesVersion": management_template['clusterConfig']['kubernetesVersion']
                        },
                    "description": management_template['description'],
                    "name": management_template['name']
                }

                if 'tags' in management_template.keys():
                    cluster_tags = []
                    for tag in management_template['tags']:
                        cluster_tags.append({'autoCreated': False, 'name': tag})
                    management_template_body['tags'] = cluster_tags

                if 'masterNodes' in management_template.keys():
                    master_nodes = []
                    for node in management_template['masterNodes']:
                        master_node_body = {
                                "cpu": node['cpu'],
                                "memory": node['memory'],
                                "name": node['name'],
                                "networks": node['networks'],
                                "storage": node['storage'],
                                "replica": node['replica'],
                            }
                        if node['cloneMode']:
                            master_node_body['cloneMode'] = 'linkedClone'
                        if 'labels' in node.keys():
                            labels = []
                            for label in node['labels']:
                                labels.append(label['key'] + '=' + label['value'])
                            master_node_body['labels'] = labels

                        master_nodes.append(master_node_body)
                    management_template_body['masterNodes'] = master_nodes

                if 'workerNodes' in management_template.keys():
                    worker_nodes = []
                    for node in management_template['workerNodes']:
                        worker_node_body = {
                                "cpu": node['cpu'],
                                "memory": node['memory'],
                                "name": node['name'],
                                "networks": node['networks'],
                                "storage": node['storage'],
                                "replica": node['replica'],
                            }
                        if node['cloneMode']:
                            worker_node_body['cloneMode'] = 'linkedClone'
                        if 'labels' in node.keys():
                            labels = []
                            for label in node['labels']:
                                labels.append(label['key'] + '=' + label['value'])
                            worker_node_body['labels'] = labels

                        worker_nodes.append(worker_node_body)
                    management_template_body['workerNodes'] = worker_nodes

                if tca_object:
                    base_http_url = base_http_url + '/' + tca_object['id']
                    resp = requests.put(base_http_url, headers=headers, data=json.dumps(management_template_body), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    resp = requests.post(base_http_url, headers=headers, data=json.dumps(management_template_body), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)

            elif operator == 'delete':
                if tca_object:
                    delete_http_url = base_http_url + '/' + tca_object['id']
                    resp = requests.delete(delete_http_url, headers=headers, verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.info('There is no such object to delete ' + str(management_template['name']))
                    continue
            else:
                logging.error('Wrong parameters')
                exit(2)

    def workload_cluster_template_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: TCA Workload Cluster Template
        """
        for workload_template in self.data['caas_templates']['workload_clusters']:
            if self.log_enable:
                logging.info('TCA Workload Cluster Template ' + str(workload_template['name']))

            base_http_url = self.tca_controller + '/hybridity/api/infra/cluster-templates'
            workload_template_resp = requests.get(base_http_url, headers=headers, verify=False)

            if workload_template_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Workload Cluster Template Query')
                exit(3)

            tca_object = TCAConfigurator.tca_object_exist_check(workload_template['name'], workload_template_resp.json(), 'name')

            # This is for rendering the POST or PATCH API Commands.
            if operator == 'create':
                workload_template_body = {
                    "clusterType": "WORKLOAD",
                    "clusterConfig": {
                            "kubernetesVersion": workload_template['clusterConfig']['kubernetesVersion']
                        },
                    "description": workload_template['description'],
                    "name": workload_template['name']
                }

                if 'tags' in workload_template.keys():
                    cluster_tags = []
                    for tag in workload_template['tags']:
                        cluster_tags.append({'autoCreated': False, 'name': tag})
                    workload_template_body['tags'] = cluster_tags

                if 'cni' in workload_template['clusterConfig'].keys():
                    cni_list = []
                    for item in workload_template['clusterConfig']['cni']:
                        cni_list.append({'name': item})
                    workload_template_body['clusterConfig']['cni'] = cni_list

                if 'csi' in workload_template['clusterConfig'].keys():
                    csi_list = []
                    for csi_item in workload_template['clusterConfig']['csi']:
                        csi = dict()
                        csi['name'] = csi_item['name']
                        csi['properties'] = dict()
                        for item in csi_item['properties'].keys():
                            csi['properties'][item] = csi_item['properties'][item]
                        csi_list.append(csi)
                    workload_template_body['clusterConfig']['csi'] = csi_list

                if 'tools' in workload_template['clusterConfig'].keys():
                    tool_list = []
                    for item in workload_template['clusterConfig']['tools']:
                        tool_list.append({'name': item['name'], 'version': item['version']})
                    workload_template_body['clusterConfig']['tools'] = tool_list

                if 'masterNodes' in workload_template.keys():
                    master_nodes = []
                    for node in workload_template['masterNodes']:
                        master_node_body = {
                                "cpu": node['cpu'],
                                "memory": node['memory'],
                                "name": node['name'],
                                "networks": node['networks'],
                                "storage": node['storage'],
                                "replica": node['replica'],
                            }
                        if node['cloneMode']:
                            master_node_body['cloneMode'] = 'linkedClone'
                        if 'labels' in node.keys():
                            labels = []
                            for label in node['labels']:
                                labels.append(label['key'] + '=' + label['value'])
                            master_node_body['labels'] = labels

                        master_nodes.append(master_node_body)
                    workload_template_body['masterNodes'] = master_nodes

                if 'workerNodes' in workload_template.keys():
                    worker_nodes = []
                    for node in workload_template['workerNodes']:
                        worker_node_body = {
                                "cpu": node['cpu'],
                                "memory": node['memory'],
                                "name": node['name'],
                                "networks": node['networks'],
                                "storage": node['storage'],
                                "replica": node['replica'],
                                "config": {}
                            }
                        worker_node_body['config']['cpuManagerPolicy'] = node['config']['cpuManagerPolicy']

                        if 'healthCheck' in node['config'].keys():
                            worker_node_body['config']['healthCheck'] = node['config']['healthCheck']

                        if node['cloneMode']:
                            worker_node_body['cloneMode'] = 'linkedClone'
                        if 'labels' in node.keys():
                            labels = []
                            for label in node['labels']:
                                labels.append(label['key'] + '=' + label['value'])
                            worker_node_body['labels'] = labels

                        worker_nodes.append(worker_node_body)
                    workload_template_body['workerNodes'] = worker_nodes

                if tca_object:
                    base_http_url = base_http_url + '/' + tca_object['id']
                    resp = requests.put(base_http_url, headers=headers, data=json.dumps(workload_template_body), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    resp = requests.post(base_http_url, headers=headers, data=json.dumps(workload_template_body), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)

            elif operator == 'delete':
                if tca_object:
                    delete_http_url = base_http_url + '/' + tca_object['id']
                    resp = requests.delete(delete_http_url, headers=headers, verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.info('There is no such object to delete ' + str(workload_template['name']))
                    continue
            else:
                logging.error('Wrong parameters')
                exit(2)

    def management_cluster_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: TCA Management Cluster
        """
        for management_cluster in self.data['caas_deployments']['management_clusters']:
            if self.log_enable:
                logging.info('TCA Management Cluster ' + str(management_cluster['name']))

            base_http_url = self.tca_controller + '/hybridity/api/infra/k8s/clusters'
            cluster_template_url = self.tca_controller + '/hybridity/api/infra/cluster-templates'
            vim_url = self.tca_controller + '/hybridity/api/vims/v1/tenants'

            management_cluster_resp = requests.get(base_http_url, headers=headers, verify=False)
            if management_cluster_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Management Cluster Query')
                exit(3)

            cluster_template_resp = requests.get(cluster_template_url, headers=headers, verify=False)
            if cluster_template_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Management Cluster Template Query')
                exit(3)

            vim_resp = requests.get(vim_url, headers=headers, verify=False)
            if vim_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Management VIM Query')
                exit(3)

            tca_object = TCAConfigurator.tca_object_exist_check(management_cluster['name'], management_cluster_resp.json(), 'clusterName')
            management_template_object = TCAConfigurator.tca_object_exist_check(management_cluster['cluster_template'], cluster_template_resp.json(), 'name')
            if not management_template_object:
                logging.error('Please check the Management Cluster Template. There is no such Management Cluster Template -> ' + management_cluster['cluster_template'])
                exit(4)

            vim_object = TCAConfigurator.vim_object_exist_check(management_cluster['vim_name'], vim_resp.json(), 'vimName')
            if not vim_object:
                logging.error('Please check the VIM. There is no such VIM -> ' + management_cluster['vim_name'])
                exit(4)

            # This is for rendering the POST or PATCH API Commands.
            if operator == 'create':
                management_cluster_body = {
                    "name": management_cluster['name'],
                    "clusterType": "MANAGEMENT",
                    "description": management_cluster['description'],
                    "vmTemplate": management_cluster['vm_template'],
                    "endpointIP": management_cluster['vip_ip'],
                    "placementParams": management_cluster['placementParams'],
                    "clusterConfig": management_cluster['clusterConfig'],
                    "clusterPassword": management_cluster['clusterPassword'],
                    "masterNodes": management_cluster['masterNodes'],
                    "workerNodes": management_cluster['workerNodes'],
                    "clusterTemplateId": management_template_object['id'],
                    "hcxCloudUrl": vim_object['hcxCloudUrl'],
                    "vimId": vim_object['vimId'],
                }

                if 'airgapConfig' in management_cluster.keys():
                    repository_url = self.tca_controller + '/hybridity/api/extensions'
                    repository_resp = requests.get(repository_url, headers=headers, verify=False)
                    if repository_resp.status_code != 200:
                        logging.error('Status Code is not 200 for TCA Harbor/AirGap Query')
                        exit(3)

                    repository_object = TCAConfigurator.repository_object_exist_check(
                        management_cluster['airgapConfig']['name'], repository_resp.json(), 'name')

                    management_cluster_body['airgapConfig'] = {
                        "type": management_cluster['airgapConfig']['type'],
                        "extensionId": repository_object['extensionId']
                    }

                if tca_object:
                    logging.info('TCA Management Cluster Update Feature is under development!!!')
                    # base_http_url = base_http_url + '/' + tca_object['id']
                    # resp = requests.put(base_http_url, headers=headers, data=json.dumps(management_cluster_body), verify=False)
                    # logging.info('Request Status Code ' + str(resp.status_code))
                    # if resp.status_code != 200:
                    #     logging.error('Status Code is not 200')
                    #     exit(1)
                else:
                    resp = requests.post(base_http_url, headers=headers, data=json.dumps(management_cluster_body), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                    while True:
                        management_cluster_resp = requests.get(base_http_url, headers=headers, verify=False)
                        tca_object = TCAConfigurator.tca_object_exist_check(management_cluster['name'],
                                                                            management_cluster_resp.json(), 'clusterName')
                        if tca_object['status'] == 'ACTIVE':
                            logging.info('TCA Management Cluster Deployed Successfully')
                            break
                        logging.info('TCA Management Cluster Status is ' + str(tca_object['status']))
                        time.sleep(60)

            elif operator == 'delete':
                if tca_object:
                    delete_http_url = base_http_url + '/' + tca_object['id']
                    resp = requests.delete(delete_http_url, headers=headers, verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                    while True:
                        management_cluster_resp = requests.get(base_http_url, headers=headers, verify=False)
                        tca_object = TCAConfigurator.tca_object_exist_check(management_cluster['name'],
                                                                            management_cluster_resp.json(), 'clusterName')
                        if not tca_object:
                            logging.info('TCA Management Cluster Deleted Successfully')
                            break
                        logging.info('TCA Management Cluster is still being deleted')
                        time.sleep(60)
                else:
                    logging.info('There is no such object to delete ' + str(management_cluster['name']))
                    continue
            else:
                logging.error('Wrong parameters')

    def workload_cluster_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: TCA Workload Cluster
        """
        for workload_cluster in self.data['caas_deployments']['workload_clusters']:
            if self.log_enable:
                logging.info('TCA Workload Cluster ' + str(workload_cluster['name']))

            base_http_url = self.tca_controller + '/hybridity/api/infra/k8s/clusters'
            cluster_template_url = self.tca_controller + '/hybridity/api/infra/cluster-templates'
            vim_url = self.tca_controller + '/hybridity/api/vims/v1/tenants'
            repository_url = self.tca_controller + '/hybridity/api/extensions'

            workload_cluster_resp = requests.get(base_http_url, headers=headers, verify=False)
            if workload_cluster_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Workload Cluster Query')
                exit(3)

            management_cluster_resp = requests.get(base_http_url, headers=headers, verify=False)
            if management_cluster_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Workload Cluster Query')
                exit(3)

            cluster_template_resp = requests.get(cluster_template_url, headers=headers, verify=False)
            if cluster_template_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Workload Cluster Template Query')
                exit(3)

            vim_resp = requests.get(vim_url, headers=headers, verify=False)
            if vim_resp.status_code != 200:
                logging.error('Status Code is not 200 for TCA Management VIM Query')
                exit(3)

            tca_object = TCAConfigurator.tca_object_exist_check(workload_cluster['name'], workload_cluster_resp.json(), 'clusterName')
            workload_template_object = TCAConfigurator.tca_object_exist_check(workload_cluster['cluster_template'], cluster_template_resp.json(), 'name')
            if not workload_template_object:
                logging.error('Please check the Workload Cluster Template. There is no such Workload Cluster Template -> ' + workload_cluster['cluster_template'])
                exit(4)

            management_cluster_object = TCAConfigurator.tca_object_exist_check(workload_cluster['management_cluster'], management_cluster_resp.json(), 'clusterName')
            if not management_cluster_object:
                logging.error('Please check the Management Cluster. There is no such Management Cluster -> ' + workload_cluster['management_cluster'])
                exit(4)

            vim_object = TCAConfigurator.vim_object_exist_check(workload_cluster['vim_name'], vim_resp.json(), 'vimName')
            if not vim_object:
                logging.error('Please check the VIM. There is no such VIM -> ' + workload_cluster['vim_name'])
                exit(4)

            # This is for rendering the POST or PATCH API Commands.
            if operator == 'create':
                workload_cluster_body = {
                    "name": workload_cluster['name'],
                    "clusterType": "WORKLOAD",
                    "description": workload_cluster['description'],
                    "vmTemplate": workload_cluster['vm_template'],
                    "endpointIP": workload_cluster['vip_ip'],
                    "placementParams": workload_cluster['placementParams'],
                    "clusterConfig": workload_cluster['clusterConfig'],
                    "clusterPassword": workload_cluster['clusterPassword'],
                    "masterNodes": workload_cluster['masterNodes'],
                    "workerNodes": workload_cluster['workerNodes'],
                    "clusterTemplateId": workload_template_object['id'],
                    "hcxCloudUrl": vim_object['hcxCloudUrl'],
                    "vimId": vim_object['vimId'],
                    "managementClusterId": management_cluster_object['id']
                }

                if 'airgapConfig' in workload_cluster.keys():
                    repository_resp = requests.get(repository_url, headers=headers, verify=False)
                    if repository_resp.status_code != 200:
                        logging.error('Status Code is not 200 for TCA Harbor/AirGap Query')
                        exit(3)

                    repository_object = TCAConfigurator.repository_object_exist_check(
                        workload_cluster['airgapConfig']['name'], repository_resp.json(), 'name')

                    workload_cluster_body['airgapConfig'] = {
                        "type": workload_cluster['airgapConfig']['type'],
                        "extensionId": repository_object['extensionId']
                    }

                if tca_object:
                    logging.info('TCA Workload Cluster Update Feature is under development!!!')
                    # base_http_url = base_http_url + '/' + tca_object['id']
                    # resp = requests.put(base_http_url, headers=headers, data=json.dumps(management_cluster_body), verify=False)
                    # logging.info('Request Status Code ' + str(resp.status_code))
                    # if resp.status_code != 200:
                    #     logging.error('Status Code is not 200')
                    #     exit(1)
                else:
                    resp = requests.post(base_http_url, headers=headers, data=json.dumps(workload_cluster_body), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                    while True:
                        workload_cluster_resp = requests.get(base_http_url, headers=headers, verify=False)
                        tca_object = TCAConfigurator.tca_object_exist_check(workload_cluster['name'],
                                                                            workload_cluster_resp.json(), 'clusterName')
                        if tca_object['status'] == 'ACTIVE':
                            logging.info('TCA Workload Cluster Deployed Successfully')
                            break
                        logging.info('TCA Workload Cluster Status is ' + str(tca_object['status']))
                        time.sleep(60)

            elif operator == 'delete':
                if tca_object:
                    harbor_url = repository_url + '/' + workload_cluster['clusterConfig']['tools'][0]['properties']['extensionId']
                    harbor_resp = requests.get(harbor_url, headers=headers, verify=False)
                    if harbor_resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(3)
                    harbor_registry_body = harbor_resp.json()['extensions'][0]
                    harbor_registry_index = next((i for i, x in enumerate(harbor_registry_body['vimInfo']) if x["vimName"] == workload_cluster['name']), None)
                    if isinstance(harbor_registry_index, int):
                        harbor_registry_body['vimInfo'].pop(harbor_registry_index)
                    harbor_dereq_resp = requests.post(harbor_url, headers=headers, data=json.dumps(harbor_registry_body), verify=False)
                    if harbor_dereq_resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(3)

                    vim_resp = requests.get(vim_url, headers=headers, verify=False)
                    if vim_resp.status_code != 200:
                        logging.error('Status Code is not 200 for TCA Workload VIM Query')
                        exit(3)

                    vim_object = TCAConfigurator.vim_object_exist_check(workload_cluster['name'], vim_resp.json(), 'vimName')
                    if not vim_object:
                        logging.info('Please check the VIM. There is no such VIM -> ' + workload_cluster['name'])
                    else:
                        workload_vim_url = vim_url + '/' + vim_object['tenantId']
                        vim_delete_resp = requests.delete(workload_vim_url, headers=headers, verify=False)
                        if vim_delete_resp.status_code != 200:
                            logging.error('Status Code is not 200 for TCA Workload VIM Deletion')
                            exit(3)

                    logging.info("Waiting 3min for VIM and Harbor De-registration for " + str(workload_cluster['name']))
                    time.sleep(180)
                    delete_http_url = base_http_url + '/' + tca_object['id']
                    resp = requests.delete(delete_http_url, headers=headers, verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                    while True:
                        workload_cluster_resp = requests.get(base_http_url, headers=headers, verify=False)
                        tca_object = TCAConfigurator.tca_object_exist_check(workload_cluster['name'],
                                                                            workload_cluster_resp.json(), 'clusterName')
                        if not tca_object:
                            logging.info('TCA Workload Cluster Deleted Successfully')
                            break
                        logging.info('TCA Workload Cluster is still being deleted')
                        time.sleep(60)
                else:
                    logging.info('There is no such object to delete ' + str(workload_cluster['name']))
                    continue
            else:
                logging.error('Wrong parameters')
