import yaml
import logging
import sys
from nsxt_configurator import NSXTConfigurator
import argparse

__AUTHOR__ = 'Veyis Ceylan(vmware.com)'
__VERSION__ = 0.0001


logging.basicConfig(stream=sys.stdout,
                    format='%(asctime)s: %(module)s: %(funcName)s:  [%(levelname)s]: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)


def main():
    argp = argparse.ArgumentParser(description="Get Start and Parse Argument")

    argp.add_argument('-v', '--verbose',
                      action="store_true",
                      help='Enable logging')

    argp.add_argument('-nu', '--nsx-user',
                      required=True,
                      help='NSX-T Manager Username')

    argp.add_argument('-np', '--nsx-password',
                      required=True,
                      help='NSX-T Manager Password')

    argp.add_argument('-sd', '--site-data',
                      required=True,
                      help='Filepath to the NSX-T Site Data')

    argp.add_argument('-req', '--request',
                      choices=['requests', 'curl'],
                      default='curl',
                      help='Requests vs Curl')

    argp.add_argument('-op', '--operator',
                      choices=['create', 'delete'],
                      default='create',
                      help='Create vs Delete')

    args = argp.parse_args(sys.argv[1:])

    with open(args.site_data, 'r') as f:
        siteconfig = yaml.full_load(f)
        logging.info("Site Variables File has been read")

    method = args.request
    operator = args.operator
    nsx_manager = siteconfig['site']['nsx_manager']
    data = siteconfig['site']

    NSXT_Config_Engine = NSXTConfigurator(nsx_manager=nsx_manager, nsx_manager_user=args.nsx_user,
                                          nsx_manager_password=args.nsx_password, data=data, log_enable=args.verbose)

    if operator == 'create':
        logging.info("NSX-T Config Creation Started")
        NSXT_Config_Engine.vlan_segment_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_interface_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_community_list_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_prefix_list_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_route_map_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_bgp_peering_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier1_configurator(method=method, operator=operator)
        NSXT_Config_Engine.overlay_segment_configurator(method=method, operator=operator)
        NSXT_Config_Engine.snat_configurator(method=method, operator=operator)

    else:
        logging.info("NSX-T Config Deletion Started")
        NSXT_Config_Engine.snat_configurator(method=method, operator=operator)
        NSXT_Config_Engine.overlay_segment_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier1_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_bgp_peering_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_route_map_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_prefix_list_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_community_list_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_interface_configurator(method=method, operator=operator)
        NSXT_Config_Engine.tier0_configurator(method=method, operator=operator)
        NSXT_Config_Engine.vlan_segment_configurator(method=method, operator=operator)


if __name__ == "__main__":
    main()
