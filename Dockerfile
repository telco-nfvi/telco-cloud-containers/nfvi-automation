FROM python:3.10

ARG APP_DIR=${APP_DIR:-/nfvi-automation}
ENV APP_DIR=${APP_DIR}

RUN pip install --upgrade pip
COPY  .  ${APP_DIR}
WORKDIR  ${APP_DIR}

RUN  python3.10 -m pip install --user -r requirements.txt \
     && rm -f /bin/sh; ln -s /bin/bash /bin/sh

ENTRYPOINT ["python3.10"]
