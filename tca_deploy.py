import yaml
import logging
import sys
from tca_configurator import TCAConfigurator
import argparse

__AUTHOR__ = 'Veyis Ceylan(vmware.com)'
__VERSION__ = 0.0001


logging.basicConfig(stream=sys.stdout,
                    format='%(asctime)s: %(module)s: %(funcName)s:  [%(levelname)s]: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)


def main():
    argp = argparse.ArgumentParser(description="Get Start and Parse Argument")

    argp.add_argument('-v', '--verbose',
                      action="store_true",
                      help='Enable logging')

    argp.add_argument('-tu', '--tca-user',
                      required=True,
                      help='TCA Manager Username')

    argp.add_argument('-tp', '--tca-password',
                      required=True,
                      help='TCA Manager Password')

    argp.add_argument('-sd', '--site-data',
                      required=True,
                      help='Filepath to the TCA Site Data')

    argp.add_argument('-req', '--request',
                      choices=['requests'],
                      default='requests',
                      help='Requests')

    argp.add_argument('-op', '--operator',
                      choices=['create', 'delete'],
                      default='create',
                      help='Create vs Delete')

    args = argp.parse_args(sys.argv[1:])

    with open(args.site_data, 'r') as f:
        siteconfig = yaml.full_load(f)
        logging.info("Site Variables File has been read")

    method = args.request
    operator = args.operator
    tca_controller = siteconfig['site']['tca_controller']
    data = siteconfig['site']

    TCA_Config_Engine = TCAConfigurator(tca_controller=tca_controller, tca_controller_user=args.tca_user,
                                        tca_controller_password=args.tca_password, data=data, log_enable=args.verbose)

    if operator == 'create':
        logging.info("TCA Config Creation Started")
        TCA_Config_Engine.management_cluster_template_configurator(method=method, operator=operator)
        TCA_Config_Engine.workload_cluster_template_configurator(method=method, operator=operator)
        TCA_Config_Engine.management_cluster_configurator(method=method, operator=operator)
        TCA_Config_Engine.workload_cluster_configurator(method=method, operator=operator)

    else:
        logging.info("TCA Config Deletion Started")
        TCA_Config_Engine.workload_cluster_configurator(method=method, operator=operator)
        TCA_Config_Engine.management_cluster_configurator(method=method, operator=operator)
        TCA_Config_Engine.workload_cluster_template_configurator(method=method, operator=operator)
        TCA_Config_Engine.management_cluster_template_configurator(method=method, operator=operator)


if __name__ == "__main__":
    main()
