#!/usr/bin/python3
import sys
import logging
import json
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning


__AUTHOR__ = "Veyis Ceylan(vceylan@vmware.com)"
__VERSION__ = 0.0001

logging.basicConfig(stream=sys.stdout,
                    format='%(asctime)s: %(module)s:  %(funcName)s:  [%(levelname)s]: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)

headers = CaseInsensitiveDict()
headers["Content-Type"] = "application/json"
headers["Accept"] = "application/json"
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class NSXTConfigurator:
    """
       Parameters for NSXTConfigurator Class
         nsx_manager = The URL for NSX-T Manager
         nsx_manager_user = The NSX-T Manager User
         nsx_manager_password = The NSX-T Manager Password
         data = The input variables in YAML format for rendering API calls for NSX-T Objects
         log_enable = Enable logging while rendering API calls for NSX-T Objects
    """
    def __init__(self, nsx_manager=None, nsx_manager_user=None, nsx_manager_password=None, data=None, log_enable=False):
        self.nsx_manager = nsx_manager
        self.nsx_manager_user = nsx_manager_user
        self.nsx_manager_password = nsx_manager_password
        self.data = data
        self.log_enable = log_enable
        logging.info("NSXT Configurator Agent is created")

    def snat_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T SNAT Rules for Tier1 Router
        """
        for tkg_node in self.data['tkg_clusters']:
            if self.log_enable:
                logging.info('SNAT Rules for ' + tkg_node['tkg_cluster_name'] + ' Cluster')

            for snat_rules in tkg_node['tkg_cnf_snat_ip']:
                if self.log_enable:
                    logging.info('SNAT Rules for ' + snat_rules['vrf'].upper() + ' VRF')
                count = 1
                for route in self.data['summary_rt'][snat_rules['vrf']]['routes']:
                    snat_id = tkg_node['tkg_cluster_name'] + '_' + snat_rules['vrf'].upper() + '_SNAT_Rule' + str(count)
                    http_url = self.nsx_manager + '/policy/api/v1/infra/tier-1s/' + tkg_node['tkg_cluster_tier1'] + '/nat/USER/nat-rules/' + snat_id
                    snat_http_body = {
                        "display_name": snat_id,
                        "action": "SNAT",
                        "source_network": tkg_node['tkg_cluster_subnet'],
                        "destination_network": route,
                        "translated_network": snat_rules['snat_ip'],
                        "enabled": "true",
                        "logging": "false",
                        "sequence_number": 10,
                        "firewall_match": "BYPASS"}
                    count += 1
                    if self.log_enable:
                        logging.info('Source Subnet ' + tkg_node['tkg_cluster_subnet'] + ' Destination Subnet ' + route + ' SNAT IP ' + snat_rules['snat_ip'])

                    # This is for rendering the PATCH API Commands.
                    if operator == 'create':
                        if method == 'curl':
                            snat_api_call = 'curl -s -k -X PATCH -u ' + "'" + self.nsx_manager_user + ':' + self.nsx_manager_password + "'" + \
                                            ' -H "Content-Type: application/json" ' + http_url + ' -d ' + "'" + json.dumps(snat_http_body) + "'"
                            print(snat_api_call)
                        elif method == 'requests':
                            resp = requests.patch(http_url, headers=headers, data=json.dumps(snat_http_body),
                                                  auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                                  verify=False)
                            logging.info('Request Status Code ' + str(resp.status_code))
                            if resp.status_code != 200:
                                logging.error('Status Code is not 200')
                                exit(1)
                        else:
                            logging.error('Wrong parameters')
                            exit(2)

                    # This is for rendering the DELETE API Commands.
                    elif operator == 'delete':
                        if method == 'curl':
                            snat_api_call = 'curl -s -k -X DELETE -u ' + "'" + self.nsx_manager_user + ':' + self.nsx_manager_password + "'"\
                                            ' -H "Content-Type: application/json" ' + http_url
                            print(snat_api_call)
                        elif method == 'requests':
                            resp = requests.delete(http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                                   verify=False)
                            logging.info('Request Status Code ' + str(resp.status_code))
                            if resp.status_code != 200:
                                logging.error('Status Code is not 200')
                                exit(1)
                        else:
                            logging.error('Wrong parameters')
                            exit(2)
                    else:
                        logging.error('Wrong parameters')
                        exit(3)

    def vlan_segment_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T VLAN Segment
        """
        for vlan in self.data['vlan_segments']['vlans']:
            if self.log_enable:
                logging.info('VLAN Backed Segment for VLAN_ID ' + str(vlan['vlan_id']))

            http_url = self.nsx_manager + '/policy/api/v1/infra/segments/' + self.data['name'] + '-T-SEG-VLAN-T0-UPLINK-' + str(vlan['vlan_id'])
            transport_zone_path = '/infra/sites/default/enforcement-points/default/transport-zones/' + self.data['vlan_segments']['segment_config']['transport_zones']

            vlan_seg_http_body = {
                "transport_zone_path": transport_zone_path,
                "vlan_ids": [str(vlan['vlan_id'])],
                # "advanced_config": {"uplink_teaming_policy_name": vlan['teaming']}
            }

            # This is for rendering the PATCH API Commands.
            if operator == 'create':
                if method == 'curl':
                    vlan_segment_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                    '\' -H "Content-Type: application/json" ' + http_url + ' -d ' + "'" + json.dumps(vlan_seg_http_body) + "'"
                    print(vlan_segment_api_call)
                elif method == 'requests':
                    resp = requests.patch(http_url, headers=headers, data=json.dumps(vlan_seg_http_body),
                                          auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)

            # This is for rendering the DELETE API Commands.
            elif operator == 'delete':
                if method == 'curl':
                    vlan_segment_api_call = 'curl -s -k -X DELETE -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                            '\' -H "Content-Type: application/json" ' + http_url
                    print(vlan_segment_api_call)
                elif method == 'requests':
                    resp = requests.delete(http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                           verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)
            else:
                logging.error('Wrong parameters')
                exit(3)

    def tier1_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Tier1 Router
        """
        for tier1 in self.data['tier1_routers']['routers']:
            if self.log_enable:
                logging.info('Tier1 Router ' + tier1['name'])

            http_url = self.nsx_manager + '/policy/api/v1/infra/tier-1s/' + tier1['name']
            patch_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-1s/' + tier1['name'] + '/locale-services/default'

            new_t1_http_body = {
                "route_advertisement_types": tier1['route_advert_type'],
                "tier0_path": "/infra/tier-0s/" + tier1['tier0']

            }

            patch_t1_http_body = {
                "edge_cluster_path": '/infra/sites/default/enforcement-points/default/edge-clusters/' + tier1['connected_edge']
            }

            # This is for rendering the PATCH API Commands.
            if operator == 'create':
                if method == 'curl':
                    tier1_new_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                         '\' -H "Content-Type: application/json" ' + http_url + ' -d ' + "'" + json.dumps(new_t1_http_body) + "'"

                    tier1_patch_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + patch_http_url + ' -d ' + "'" + json.dumps(patch_t1_http_body) + "'"

                    print(tier1_new_api_call)
                    print(tier1_patch_api_call)
                elif method == 'requests':
                    resp1 = requests.patch(http_url, headers=headers, data=json.dumps(new_t1_http_body),
                                           auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                    resp2 = requests.patch(patch_http_url, headers=headers, data=json.dumps(patch_t1_http_body),
                                           auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                    logging.info('Request Status Code ' + str(resp1.status_code))
                    logging.info('Request Status Code ' + str(resp2.status_code))
                    if resp1.status_code != 200 or resp2.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)

            # This is for rendering the DELETE API Commands.
            elif operator == 'delete':
                if method == 'curl':
                    tier1_new_api_call = 'curl -s -k -X DELETE -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                         '\' -H "Content-Type: application/json" ' + http_url

                    tier1_patch_api_call = 'curl -s -k -X DELETE -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + patch_http_url
                    print(tier1_patch_api_call)
                    print(tier1_new_api_call)
                elif method == 'requests':
                    resp1 = requests.delete(patch_http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                            verify=False)
                    resp2 = requests.delete(http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                            verify=False)
                    logging.info('Request Status Code ' + str(resp1.status_code))
                    logging.info('Request Status Code ' + str(resp2.status_code))
                    if resp1.status_code != 200 or resp2.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)
            else:
                logging.error('Wrong parameters')
                exit(3)

    def overlay_segment_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Overlay Segment
        """
        for tier1 in self.data['tier1_routers']['routers']:
            if self.log_enable:
                logging.info('Overlay Backed Segments for Tier1 Router  ' + str(tier1['name']))
            for overlay_segment in tier1['segments']:
                if self.log_enable:
                    logging.info('Overlay Backed Segment  ' + str(overlay_segment['name']))
                http_url = self.nsx_manager + '/policy/api/v1/infra/segments/' + overlay_segment['name']
                transport_zone_path = '/infra/sites/default/enforcement-points/default/transport-zones/' + \
                                      self.data['tier1_routers']['overlay_segments']['overlay_transport_zone']

                overlay_segment_http_body = {
                    "subnets": [{"gateway_address": overlay_segment['gateway']}],
                    "transport_zone_path": transport_zone_path,
                    "connectivity_path": "/infra/tier-1s/" + tier1['name']
                }

                if overlay_segment['dhcp_enabled']:
                    overlay_segment_http_body['subnets'][0]['dhcp_ranges'] = [overlay_segment['dhcp_range']]
                    overlay_segment_http_body['subnets'][0]['dhcp_config'] = {"resource_type": self.data['tier1_routers']['overlay_segments']['dhcp_config']['overlay_resource_type'],
                                                                              "lease_time": self.data['tier1_routers']['overlay_segments']['dhcp_config']['overlay_lease_time'],
                                                                              "server_address": overlay_segment['dhcp_server'],
                                                                              "dns_servers": self.data['tier1_routers']['overlay_segments']['dhcp_config']['overlay_dns_server']}
                    overlay_segment_http_body['dhcp_config_path'] = '/infra/dhcp-server-configs/' + self.data['tier1_routers']['overlay_segments']['dhcp_config']['overlay_dhcp_config_path']

                # This is for rendering the PATCH API Commands.
                if operator == 'create':
                    if method == 'curl':
                        overlay_segment_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                                '\' -H "Content-Type: application/json" ' + http_url + ' -d ' + "'" + json.dumps(overlay_segment_http_body) + "'"
                        print(overlay_segment_api_call)
                    elif method == 'requests':
                        resp = requests.patch(http_url, headers=headers, data=json.dumps(overlay_segment_http_body),
                                              auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                              verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)

                # This is for rendering the DELETE API Commands.
                elif operator == 'delete':
                    if method == 'curl':
                        overlay_segment_api_call = 'curl -s -k -X DELETE -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                                '\' -H "Content-Type: application/json" ' + http_url
                        print(overlay_segment_api_call)
                    elif method == 'requests':
                        resp = requests.delete(http_url,
                                               auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                               verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)
                else:
                    logging.error('Wrong parameters')
                    exit(3)

    def tier0_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Tier0 Router
        """
        for tier0 in self.data['tier0_routers']['routers']:
            if self.log_enable:
                logging.info('Tier0 Router ' + tier0['name'])

            http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name']
            cluster_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name'] + '/locale-services/default'
            bgp_enable_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name'] + '/locale-services/default/bgp'

            new_vrf_http_body = {
                "vrf_config": {"tier0_path": "/infra/tier-0s/" + tier0['parent_t0']},
                "transit_subnets": self.data['tier0_routers']['t0_t1_transit_subnet'],
                "id": tier0['name']
            }

            cluster_vrf_http_body = {
                "route_redistribution_config": {
                    "bgp_enabled": "true",
                    "redistribution_rules": [
                        {
                            "name": tier0['route_redistribution_rule_name'],
                            "route_redistribution_types": tier0['route_redistribution_type'],
                            "destinations": [
                                "BGP"
                            ]
                        }
                    ]},
                "edge_cluster_path": '/infra/sites/default/enforcement-points/default/edge-clusters/' + tier0['connected_edge']
            }

            enable_bgp_http_body = {
                "enabled": "true",
                "ecmp": "true"
            }

            # This is for rendering the PATCH API Commands.
            if operator == 'create':
                if method == 'curl':
                    new_vrf_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                       '\' -H "Content-Type: application/json" ' + http_url + ' -d ' + "'" + json.dumps(new_vrf_http_body) + "'"

                    cluster_patch_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                             '\' -H "Content-Type: application/json" ' + cluster_http_url + ' -d ' + "'" + json.dumps(cluster_vrf_http_body) + "'"

                    enable_bgp_patch_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                                '\' -H "Content-Type: application/json" ' + bgp_enable_http_url + ' -d ' + "'" + json.dumps(enable_bgp_http_body) + "'"

                    print(new_vrf_api_call)
                    print(cluster_patch_api_call)
                    print(enable_bgp_patch_api_call)
                elif method == 'requests':
                    resp1 = requests.patch(http_url, headers=headers, data=json.dumps(new_vrf_http_body),
                                           auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                    resp2 = requests.patch(cluster_http_url, headers=headers, data=json.dumps(cluster_vrf_http_body),
                                           auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                    resp3 = requests.patch(bgp_enable_http_url, headers=headers, data=json.dumps(enable_bgp_http_body),
                                           auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                    logging.info('Request Status Code ' + str(resp1.status_code))
                    logging.info('Request Status Code ' + str(resp2.status_code))
                    logging.info('Request Status Code ' + str(resp3.status_code))
                    if resp1.status_code != 200 or resp2.status_code != 200 or resp3.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)

            # This is for rendering the DELETE API Commands.
            elif operator == 'delete':
                if method == 'curl':
                    new_vrf_api_call = 'curl -s -k -X DELETE -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                         '\' -H "Content-Type: application/json" ' + http_url

                    cluster_patch_api_call = 'curl -s -k -X DELETE -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                             '\' -H "Content-Type: application/json" ' + cluster_http_url
                    enable_bgp_patch_api_call = 'curl -s -k -X DELETE -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                                '\' -H "Content-Type: application/json" ' + bgp_enable_http_url
                    print(enable_bgp_patch_api_call)
                    print(cluster_patch_api_call)
                    print(new_vrf_api_call)
                elif method == 'requests':
                    resp1 = requests.delete(cluster_http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                            verify=False)
                    resp2 = requests.delete(http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                            verify=False)
                    logging.info('Request Status Code ' + str(resp1.status_code))
                    logging.info('Request Status Code ' + str(resp2.status_code))
                    if resp1.status_code != 200 or resp2.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)
            else:
                logging.error('Wrong parameters')
                exit(3)

    def tier0_interface_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Tier0 Router Interface
        """
        for tier0 in self.data['tier0_routers']['routers']:
            if self.log_enable:
                logging.info('Tier0 Router ' + tier0['name'])

            for interface in tier0['interfaces']:
                if self.log_enable:
                    logging.info('Tier0 Router Interface ' + interface['name'])

                interface_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name'] + '/locale-services/default/interfaces/' + interface['name']

                interface_http_body = {
                    "segment_path": "/infra/segments/" + interface['vlan_segment'],
                    "subnets": [
                        {
                            "ip_addresses": [interface['int_address'].split('/')[0]],
                            "prefix_len": interface['int_address'].split('/')[1]
                        }
                    ],
                    "edge_path": '/infra/sites/default/enforcement-points/default/edge-clusters/' + tier0['connected_edge'] + '/edge-nodes/' + str(interface['edge_node']),
                    "mtu": interface['int_mtu'],
                    "type": "EXTERNAL"
                }

                # This is for rendering the PATCH API Commands.
                if operator == 'create':
                    if method == 'curl':
                        interface_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + interface_http_url + ' -d ' + "'" + json.dumps(interface_http_body) + "'"

                        print(interface_api_call)
                    elif method == 'requests':
                        resp = requests.patch(interface_http_url, headers=headers, data=json.dumps(interface_http_body),
                                              auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)

                # This is for rendering the DELETE API Commands.
                elif operator == 'delete':
                    if method == 'curl':
                        interface_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + interface_http_url

                        print(interface_api_call)
                    elif method == 'requests':
                        resp = requests.delete(interface_http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                               verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)
                else:
                    logging.error('Wrong parameters')
                    exit(3)

    def tier0_community_list_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Tier0 Router Community List
        """
        for tier0 in self.data['tier0_routers']['routers']:
            if self.log_enable:
                logging.info('Tier0 Router ' + tier0['name'])

            for community in tier0['bgp_community']:
                if self.log_enable:
                    logging.info('Tier0 Router Community List ' + community['name'])

                community_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name'] + '/community-lists/' + community['name']

                community_http_body = {
                    "communities": community['community_list']
                }

                # This is for rendering the PATCH API Commands.
                if operator == 'create':
                    if method == 'curl':
                        community_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + community_http_url + ' -d ' + "'" + json.dumps(community_http_body) + "'"

                        print(community_api_call)
                    elif method == 'requests':
                        resp = requests.patch(community_http_url, headers=headers, data=json.dumps(community_http_body),
                                              auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)

                # This is for rendering the DELETE API Commands.
                elif operator == 'delete':
                    if method == 'curl':
                        community_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + community_http_url

                        print(community_api_call)
                    elif method == 'requests':
                        resp = requests.delete(community_http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                               verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)
                else:
                    logging.error('Wrong parameters')
                    exit(3)

    def tier0_prefix_list_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Tier0 Router Prefix List
        """
        for tier0 in self.data['tier0_routers']['routers']:
            if self.log_enable:
                logging.info('Tier0 Router ' + tier0['name'])

            for prefix_list in tier0['prefix_list']:
                if self.log_enable:
                    logging.info('Tier0 Router Prefix List ' + prefix_list['name'])

                prefix_list_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name'] + '/prefix-lists/' + prefix_list['name']

                prefix_list_temp = []
                for prefix in prefix_list['prefixes']:
                    prefix_list_temp.append({"network": prefix['subnet'], "action": prefix['action']})

                prefix_list_http_body = {
                    "prefixes": prefix_list_temp
                }

                # This is for rendering the PATCH API Commands.
                if operator == 'create':
                    if method == 'curl':
                        prefix_list_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + prefix_list_http_url + ' -d ' + "'" + json.dumps(prefix_list_http_body) + "'"

                        print(prefix_list_api_call)
                    elif method == 'requests':
                        resp = requests.patch(prefix_list_http_url, headers=headers, data=json.dumps(prefix_list_http_body),
                                              auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)

                # This is for rendering the DELETE API Commands.
                elif operator == 'delete':
                    if method == 'curl':
                        prefix_list_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + prefix_list_http_url

                        print(prefix_list_api_call)
                    elif method == 'requests':
                        resp = requests.delete(prefix_list_http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                               verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)
                else:
                    logging.error('Wrong parameters')
                    exit(3)

    def tier0_route_map_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Tier0 Router Route Map
        """
        for tier0 in self.data['tier0_routers']['routers']:
            if self.log_enable:
                logging.info('Tier0 Router ' + tier0['name'])

            for route_map in tier0['route_map']:
                if self.log_enable:
                    logging.info('Tier0 Router Route Map ' + route_map['name'])

                route_map_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name'] + '/route-maps/' + route_map['name']

                route_map_http_body = {
                    "entries": []
                }

                if 'community_list_matches' in route_map['match_rules']:
                    for community_list_rule in route_map['match_rules']['community_list_matches']:
                        community_dict = {
                            "community_list_matches": [],
                            "action": community_list_rule['action']
                        }
                        for community in community_list_rule['community_list']:
                            community_dict['community_list_matches'].append({
                                "criteria": "/infra/tier-0s/" + tier0['name'] + "/community-lists/" + community['name'],
                                "match_operator": community['match_operator']
                            })
                        route_map_http_body['entries'].append(community_dict)

                if 'prefix_list_matches' in route_map['match_rules']:
                    for prefix_list_rule in route_map['match_rules']['prefix_list_matches']:
                        prefix_dict = {
                            "prefix_list_matches": [],
                            "set": {
                                "community": prefix_list_rule['community']
                            },
                            "action": prefix_list_rule['action']
                        }
                        for prefix in prefix_list_rule['prefix_list']:
                            prefix_dict['prefix_list_matches'].append("/infra/tier-0s/" + tier0['name'] + "/prefix-lists/" + prefix)
                        route_map_http_body['entries'].append(prefix_dict)

                # This is for rendering the PATCH API Commands.
                if operator == 'create':
                    if method == 'curl':
                        route_map_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + route_map_http_url + ' -d ' + "'" + json.dumps(route_map_http_body) + "'"

                        print(route_map_api_call)
                    elif method == 'requests':
                        resp = requests.patch(route_map_http_url, headers=headers, data=json.dumps(route_map_http_body),
                                              auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)

                # This is for rendering the DELETE API Commands.
                elif operator == 'delete':
                    if method == 'curl':
                        route_map_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + route_map_http_url

                        print(route_map_api_call)
                    elif method == 'requests':
                        resp = requests.delete(route_map_http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                               verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)
                else:
                    logging.error('Wrong parameters')
                    exit(3)

    def tier0_bgp_peering_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: NSX-T Tier0 Router BGP Peer
        """
        for tier0 in self.data['tier0_routers']['routers']:
            if self.log_enable:
                logging.info('Tier0 Router ' + tier0['name'])

            for bgp_peer in tier0['bgp_peering']:
                if self.log_enable:
                    logging.info('Tier0 Router BGP Peer ' + bgp_peer['neighbor_address'])

                bgp_peer_http_url = self.nsx_manager + '/policy/api/v1/infra/tier-0s/' + tier0['name'] + '/locale-services/default/bgp/neighbors/' + bgp_peer['neighbor_address']

                in_route_filters = []
                out_route_filters = []

                if bgp_peer['in_route_filters']['type'] == 'prefix-filter':
                    in_route_filters.append("/infra/tier-0s/" + tier0['name'] + "/prefix-lists/" + bgp_peer['in_route_filters']['filter'])
                else:
                    in_route_filters.append("/infra/tier-0s/" + tier0['name'] + "/route-maps/" + bgp_peer['in_route_filters']['filter'])

                if bgp_peer['out_route_filters']['type'] == 'prefix-filter':
                    out_route_filters.append("/infra/tier-0s/" + tier0['name'] + "/prefix-lists/" + bgp_peer['out_route_filters']['filter'])
                else:
                    out_route_filters.append("/infra/tier-0s/" + tier0['name'] + "/route-maps/" + bgp_peer['out_route_filters']['filter'])

                bgp_peer_http_body = {
                    "neighbor_address": bgp_peer['neighbor_address'],
                    "remote_as_num": self.data['tier0_routers']['bgp_remote_as_num'],
                    "password": self.data['tier0_routers']['bgp_password'],
                    "allow_as_in": bgp_peer['allow_as_in'],
                    "source_addresses": bgp_peer['source_addresses'],
                    "route_filtering": [{
                        "address_family": "IPV4",
                        "enabled": "true",
                        "in_route_filters": in_route_filters,
                        "out_route_filters": out_route_filters
                    }],
                    "graceful_restart_mode": "HELPER_ONLY",
                    "bfd": {
                        "enabled": 'true',
                        "multiple": self.data['tier0_routers']['bgp_bfd_multiplier'],
                        "interval": self.data['tier0_routers']['bgp_bfd_interval']
                    }
                }

                # This is for rendering the PATCH API Commands.
                if operator == 'create':
                    if method == 'curl':
                        bgp_peer_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + bgp_peer_http_url + ' -d ' + "'" + json.dumps(bgp_peer_http_body) + "'"

                        print(bgp_peer_api_call)
                    elif method == 'requests':
                        resp = requests.patch(bgp_peer_http_url, headers=headers, data=json.dumps(bgp_peer_http_body),
                                              auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password), verify=False)
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)

                # This is for rendering the DELETE API Commands.
                elif operator == 'delete':
                    if method == 'curl':
                        bgp_peer_api_call = 'curl -s -k -X PATCH -u \'' + self.nsx_manager_user + ':' + self.nsx_manager_password + \
                                           '\' -H "Content-Type: application/json" ' + bgp_peer_http_url

                        print(bgp_peer_api_call)
                    elif method == 'requests':
                        resp = requests.delete(bgp_peer_http_url, auth=HTTPBasicAuth(self.nsx_manager_user, self.nsx_manager_password),
                                               verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        logging.error('Wrong parameters')
                        exit(2)
                else:
                    logging.error('Wrong parameters')
                    exit(3)
