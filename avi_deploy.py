import yaml
import logging
import sys
from avi_configurator import AVIConfigurator
import argparse

__AUTHOR__ = 'Veyis Ceylan(vmware.com)'
__VERSION__ = 0.0001


logging.basicConfig(stream=sys.stdout,
                    format='%(asctime)s: %(module)s: %(funcName)s:  [%(levelname)s]: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)


def main():
    argp = argparse.ArgumentParser(description="Get Start and Parse Argument")

    argp.add_argument('-v', '--verbose',
                      action="store_true",
                      help='Enable logging')

    argp.add_argument('-au', '--avi-user',
                      required=True,
                      help='AVI Controller Username')

    argp.add_argument('-ap', '--avi-password',
                      required=True,
                      help='AVI Controller Password')

    argp.add_argument('-sd', '--site-data',
                      required=True,
                      help='Filepath to the AVI Site Data')

    argp.add_argument('-req', '--request',
                      choices=['requests', 'curl'],
                      default='curl',
                      help='Requests vs Curl')

    argp.add_argument('-op', '--operator',
                      choices=['create', 'delete'],
                      default='create',
                      help='Create vs Delete')

    args = argp.parse_args(sys.argv[1:])

    with open(args.site_data, 'r') as f:
        siteconfig = yaml.full_load(f)
        logging.info("Site Variables File has been read")

    method = args.request
    operator = args.operator
    avi_controller = siteconfig['site']['avi_controller']
    data = siteconfig['site']

    AVI_Config_Engine = AVIConfigurator(avi_controller=avi_controller, avi_controller_user=args.avi_user,
                                        avi_controller_password=args.avi_password, data=data, log_enable=args.verbose)

    if operator == 'create':
        logging.info("AVI Config Creation Started")
        AVI_Config_Engine.se_group_configurator(method=method, operator=operator)
        AVI_Config_Engine.network_pool_configurator(method=method, operator=operator)

    else:
        logging.info("AVI Config Deletion Started")
        AVI_Config_Engine.network_pool_configurator(method=method, operator=operator)
        AVI_Config_Engine.se_group_configurator(method=method, operator=operator)


if __name__ == "__main__":
    main()
