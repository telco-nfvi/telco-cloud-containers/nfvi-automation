#!/usr/bin/python3
import sys
import logging
import json
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning


__AUTHOR__ = "Veyis Ceylan(vceylan@vmware.com)"
__VERSION__ = 0.0001

logging.basicConfig(stream=sys.stdout,
                    format='%(asctime)s: %(module)s:  %(funcName)s:  [%(levelname)s]: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)

headers = CaseInsensitiveDict()
headers["Content-Type"] = "application/json"
headers["Accept"] = "application/json"
headers["X-Avi-Version"] = "20.1.5"
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class AVIConfigurator:
    """
       Parameters for AVIConfigurator Class
         avi_controller = The URL for AVI Controller
         avi_controller_user = The AVI Controller User
         avi_controller_password = The AVI Controller Password
         data = The input variables in YAML format for rendering API calls for AVI Objects
         log_enable = Enable logging while rendering API calls for AVI Objects
    """
    def __init__(self, avi_controller=None, avi_controller_user=None, avi_controller_password=None, data=None, log_enable=False):
        self.avi_controller = avi_controller
        self.avi_controller_user = avi_controller_user
        self.avi_controller_password = avi_controller_password
        self.data = data
        self.log_enable = log_enable
        logging.info("AVI Configurator Agent is created")

    def se_group_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: AVI SE Group
        """
        for se_group in self.data['se_groups']:
            if self.log_enable:
                logging.info('AVI SE Group ' + str(se_group['name']))

            cloud_ref = ""
            tenant_ref = self.avi_controller + '/api/tenant/' + se_group['tenant_ref']
            cloud_url = self.avi_controller + '/api/cloud?name=' + se_group['cloud_ref']
            base_http_url = self.avi_controller + '/api/serviceenginegroup'
            cloud_resp = requests.get(cloud_url, headers=headers, auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password),
                                      verify=False)

            if cloud_resp.status_code != 200:
                logging.error('Status Code is not 200 for Cloud Query')
                exit(4)
            else:
                if cloud_resp.json()['count'] == 1:
                    cloud_ref = cloud_resp.json()['results'][0]['url']
                else:
                    logging.error('There is something wrong with Cloud URL Ref Collection')
                    exit(5)

            data_stores = []
            for datastore in se_group['vcenter_datastores']:
                data_stores.append({"datastore_name": datastore})

            vsphere_clusters = []
            for cluster in se_group['vcenter_clusters']:
                cluster_url = self.avi_controller + '/api/vimgrclusterruntime?name=' + cluster
                cluster_resp = requests.get(cluster_url, headers=headers,
                                            auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password),
                                            verify=False)
                if cluster_resp.status_code != 200:
                    logging.error('Status Code is not 200 for vSphere Cluster Query')
                    exit(4)
                else:
                    if cluster_resp.json()['count'] == 1:
                        vsphere_clusters.append(cluster_resp.json()['results'][0]['url'])
                    else:
                        logging.error('There is something wrong with vSphere Cluster Ref Collection')
                        exit(5)

            se_group_http_body = {
                "active_standby": False,
                "aggressive_failure_detection": False,
                "algo": "PLACEMENT_ALGO_DISTRIBUTED",
                "async_ssl": False,
                "async_ssl_threads": 1,
                "auto_rebalance": False,
                "auto_rebalance_interval": 300,
                "auto_redistribute_active_standby_load": False,
                "buffer_se": 1,
                "cloud_ref": cloud_ref,
                "connection_memory_percentage": 50,
                "cpu_reserve": False,
                "cpu_socket_affinity": False,
                "dedicated_dispatcher_core": False,
                "disk_per_se": 15,
                "distribute_load_active_standby": False,
                "extra_config_multiplier": 0.0,
                "ha_mode": "HA_MODE_SHARED",
                "hm_on_standby": True,
                "least_load_core_selection": True,
                "log_disksz": 10000,
                "max_cpu_usage": 80,
                "max_scaleout_per_vs": 2,
                "max_se": 10,
                "max_vs_per_se": 100,
                "mem_reserve": True,
                "memory_per_se": 2048,
                "min_cpu_usage": 30,
                "min_scaleout_per_vs": 1,
                "name": se_group['name'],
                "num_flow_cores_sum_changes_to_ignore": 8,
                "os_reserved_memory": 0,
                "per_app": False,
                "placement_mode": "PLACEMENT_MODE_AUTO",
                "realtime_se_metrics": {
                    "duration": 30,
                    "enabled": False
                },
                "se_deprovision_delay": 120,
                "se_dos_profile": {
                    "thresh_period": 5
                },
                "se_name_prefix": se_group['se_prefix'],
                "se_thread_multiplier": 1,
                "tenant_ref": tenant_ref,
                "vcenter_clusters": {
                    "include": True,
                    "cluster_refs": vsphere_clusters
                },
                "vcenter_datastore_mode": "VCENTER_DATASTORE_SHARED",
                "vcenter_datastores": data_stores,
                "vcenter_datastores_include": True,
                "vcenter_folder": se_group['se_folder'],
                "vcpus_per_se": 1,
                "vs_host_redundancy": True,
                "vs_scalein_timeout": 30,
                "vs_scalein_timeout_for_upgrade": 30,
                "vs_scaleout_timeout": 600
            }
            patch_se_group_http_body = dict()
            patch_se_group_http_body['replace'] = se_group_http_body

            # This is for rendering the POST or PATCH API Commands.
            if operator == 'create':
                # Checking whether SE Group is already there or not. If there then PATCH else POST request
                se_query_url = base_http_url + '?name=' + se_group['name'] + '&' + 'cloud_ref.name=' + se_group['cloud_ref']
                http_url = ""
                se_group_check_resp = requests.get(se_query_url, headers=headers,
                                                   auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password),
                                                   verify=False)

                if se_group_check_resp.status_code == 200 and se_group_check_resp.json()['count'] == 1:
                    http_url = se_group_check_resp.json()['results'][0]['url']
                elif se_group_check_resp.status_code == 200 and se_group_check_resp.json()['count'] == 0:
                    http_url = base_http_url
                else:
                    logging.error('There is something wrong with SE Group Ref Collection')
                    exit(5)

                if method == 'curl':
                    if se_group_check_resp.json()['count'] == 1:
                        se_group_api_call = 'curl -s -k -X PATCH -u \'' + self.avi_controller_user + ':' + self.avi_controller_password + \
                                            '\' -H "Content-Type: application/json" -H "X-Avi-Version: 20.1.5" ' + http_url + ' -d ' + "'" + json.dumps(patch_se_group_http_body) + "'"
                        print(se_group_api_call)
                    else:
                        se_group_api_call = 'curl -s -k -X POST -u \'' + self.avi_controller_user + ':' + self.avi_controller_password + \
                                    '\' -H "Content-Type: application/json" -H "X-Avi-Version: 20.1.5" ' + http_url + ' -d ' + "'" + json.dumps(se_group_http_body) + "'"
                        print(se_group_api_call)

                elif method == 'requests':
                    if se_group_check_resp.json()['count'] == 1:
                        resp = requests.patch(http_url, headers=headers, data=json.dumps(patch_se_group_http_body),
                                              auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password), verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))

                        if resp.status_code != 200:
                            logging.error('Status Code is not 200')
                            exit(1)
                    else:
                        resp = requests.post(http_url, headers=headers, data=json.dumps(se_group_http_body),
                                             auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password), verify=False)
                        logging.info('Request Status Code ' + str(resp.status_code))
                        print(resp.status_code)

                        if resp.status_code != 201:
                            logging.error('Status Code is not 201')
                            exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)

            # This is for rendering the DELETE API Commands.
            elif operator == 'delete':
                # Checking whether SE Group is already there or not.
                se_query_url = base_http_url + '?name=' + se_group['name'] + '&' + 'cloud_ref.name=' + se_group['cloud_ref']
                http_url = ""
                se_group_check_resp = requests.get(se_query_url, headers=headers,
                                                   auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password),
                                                   verify=False)

                if se_group_check_resp.status_code == 200 and se_group_check_resp.json()['count'] == 1:
                    http_url = se_group_check_resp.json()['results'][0]['url']
                elif se_group_check_resp.status_code == 200 and se_group_check_resp.json()['count'] == 0:
                    # There is no SE Group to delete. Therefore, move forward with continue
                    continue
                else:
                    logging.error('There is something wrong with SE Group Ref Collection')
                    exit(5)

                if method == 'curl':
                    se_group_api_call = 'curl -s -k -X DELETE -u \'' + self.avi_controller_user + ':' + self.avi_controller_password + \
                                            '\' -H "Content-Type: application/json" -H "X-Avi-Version: 20.1.5" ' + http_url
                    print(se_group_api_call)
                elif method == 'requests':
                    resp = requests.delete(http_url, headers=headers, auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password),
                                           verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))
                    if resp.status_code != 204:
                        logging.error('Status Code is not 204')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)
            else:
                logging.error('Wrong parameters')
                exit(3)

    def network_pool_configurator(self, method=None, operator=None):
        """
        :param method: It is either curl or requests
        :param operator: it is either create or delete
        :return: AVI Network Pool for VIPs and SE
        """
        for network_pool in self.data['network_pools']:
            if self.log_enable:
                logging.info('AVI SE Group ' + str(network_pool['name']))

            http_url = ""
            network_http_url = self.avi_controller + '/api/network' + '?name=' + network_pool['name'] + '&' + 'cloud_ref.name=' + network_pool['cloud_ref']
            network_resp = requests.get(network_http_url, headers=headers, auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password),
                                        verify=False)

            if network_resp.status_code != 200:
                logging.error('Status Code is not 200 for Network Query')
                exit(4)
            else:
                if network_resp.json()['count'] == 1:
                    http_url = network_resp.json()['results'][0]['url']
                elif network_resp.json()['count'] == 0:
                    continue
                else:
                    logging.error('There is something wrong with Network Ref Collection')
                    exit(5)

            configured_subnets = []
            subnet_dict = dict()
            for subnet in network_pool['subnets']:
                subnet_dict['prefix'] = {
                    "ip_addr": {
                        "addr": subnet['subnet'].split('/')[0],
                        "type": "V4"
                    },
                    "mask": subnet['subnet'].split('/')[1]
                }
                subnet_dict['static_ip_ranges'] = []
                for pool in subnet['pools']:
                    subnet_dict['static_ip_ranges'].append({
                        "range": {
                            "begin": {
                                "addr": pool['begin'],
                                "type": "V4"
                            },
                            "end": {
                                "addr": pool['end'],
                                "type": "V4"
                            }},
                        "type": pool['pool_type']
                    })
                configured_subnets.append(subnet_dict)

            network_pool_http_body = {
                "configured_subnets": configured_subnets
            }

            patch_network_pool_http_body = dict()
            patch_network_pool_http_body['replace'] = network_pool_http_body
            patch_network_pool_http_body['replace']['exclude_discovered_subnets'] = True
            delete_network_pool_http_body = dict()
            delete_network_pool_http_body['delete'] = network_pool_http_body
            # delete_network_pool_http_body['replace'] = dict()
            # delete_network_pool_http_body['replace']['exclude_discovered_subnets'] = False

            # This is for rendering the  PATCH API Commands.
            if operator == 'create':
                if method == 'curl':
                    network_pool_api_call = 'curl -s -k -X PATCH -u \'' + self.avi_controller_user + ':' + self.avi_controller_password + \
                                            '\' -H "Content-Type: application/json" -H "X-Avi-Version: 20.1.5" ' + http_url + ' -d ' + "'" + json.dumps(patch_network_pool_http_body) + "'"
                    print(network_pool_api_call)

                elif method == 'requests':
                    resp = requests.patch(http_url, headers=headers, data=json.dumps(patch_network_pool_http_body),
                                          auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))

                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)
            elif operator == 'delete':
                if method == 'curl':
                    network_pool_api_call = 'curl -s -k -X PATCH -u \'' + self.avi_controller_user + ':' + self.avi_controller_password + \
                                            '\' -H "Content-Type: application/json" -H "X-Avi-Version: 20.1.5" ' + http_url + ' -d ' + "'" + json.dumps(delete_network_pool_http_body) + "'"
                    print(network_pool_api_call)

                elif method == 'requests':
                    resp = requests.patch(http_url, headers=headers, data=json.dumps(delete_network_pool_http_body),
                                          auth=HTTPBasicAuth(self.avi_controller_user, self.avi_controller_password), verify=False)
                    logging.info('Request Status Code ' + str(resp.status_code))

                    if resp.status_code != 200:
                        logging.error('Status Code is not 200')
                        exit(1)
                else:
                    logging.error('Wrong parameters')
                    exit(2)
            else:
                logging.error('Wrong parameters')
                exit(3)
